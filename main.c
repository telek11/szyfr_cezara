#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define W 26
#define K 26
#define S 1000000
char tab[W][K]={{"ABCDEFGHIJKLMNOPQRSTUVWXYZ"},//TABLICA NA BAZIE KTOREJ MA MIEJSCE SZYFROWANIE
                {"BCDEFGHIJKLMNOPQRSTUVWXYZA"},
                {"CDEFGHIJKLMNOPQRSTUVWXYZAB"},
                {"DEFGHIJKLMNOPQRSTUVWXYZABC"},
                {"EFGHIJKLMNOPQRSTUVWXYZABCD"},
                {"FGHIJKLMNOPQRSTUVWXYZABCDE"},
                {"GHIJKLMNOPQRSTUVWXYZABCDEF"},
                {"HIJKLMNOPQRSTUVWXYZABCDEFG"},
                {"IJKLMNOPQRSTUVWXYZABCDEFGH"},
                {"JKLMNOPQRSTUVWXYZABCDEFGHI"},
                {"KLMNOPQRSTUVWXYZABCDEFGHIJ"},
                {"LMNOPQRSTUVWXYZABCDEFGHIJK"},
                {"MNOPQRSTUVWXYZABCDEFGHIJKL"},
                {"NOPQRSTUVWXYZABCDEFGHIJKLM"},
                {"OPQRSTUVWXYZABCDEFGHIJKLMN"},
                {"PQRSTUVWXYZABCDEFGHIJKLMNO"},
                {"QRSTUVWXYZABCDEFGHIJKLMNOP"},
                {"RSTUVWXYZABCDEFGHIJKLMNOPQ"},
                {"STUVWXYZABCDEFGHIJKLMNOPQR"},
                {"TUVWXYZABCDEFGHIJKLMNOPQRS"},
                {"UVWXYZABCDEFGHIJKLMNOPQRST"},
                {"VWXYZABCDEFGHIJKLMNOPQRSTU"},
                {"WXYZABCDEFGHIJKLMNOPQRSTUV"},
                {"XYZABCDEFGHIJKLMNOPQRSTUVW"},
                {"YZABCDEFGHIJKLMNOPQRSTUVWX"},
                {"ZABCDEFGHIJKLMNOPQRSTUVWXY"}};
int i, j,k=0; // ZMIENNE UZYWANE W PROGRAMIE DO OBS�UGI PETLI
FILE *fpszyfr,*fpdozaszyfrowania,*fpklucz;//WSKAZNIKI DO PLI�W
char tekst[S];//LANCUCH ZNAKOW DO KTOREGO ZAPISYWANY JEST TEKST DO ZASZYFROWANIA
char klucz[S];//LANCUCH ZNAKOW DO KTOREGO ZAPISYWANY KLUCZ
char szyfr[S];//LANCUCH ZNAKOW DO KTOREGO ZAPISYWANY JEST ZASZYFROWANY TEKST
int wk,kt;
void szyfrowanie(void)//FUNKCJA SZYFRUJACA
{
fpdozaszyfrowania=fopen("dozaszyfrowania.txt","r");//OTWARCIE PLIKU W TRYBIE DO ODCZYTU ZAWIERAJACEGO TEKST DO ZASZYFROWANIA
if(fpdozaszyfrowania==NULL)fprintf(stderr,"Blad otwarcia pliku\n");//SPRAWDZENIE POPRAWNOSCI OTWARCIA PLIKU
else fgets(tekst,sizeof(tekst),fpdozaszyfrowania);//SPOKIOWANIE ZAWARTOSCI PLIKU DO TABLICY TEKST
fclose(fpdozaszyfrowania);//ZAMKNIECIE PLIKU

for(i=0,j=0,k=0;i<strlen(tekst);i++,j++)//PETLA W KTOREJ ODBYWA SIE SZYFROWANIE. ZMIENNA i WSKAZUJE NA KOLEJNE
                                        //ELEMENTY TEKSTU DO ZASZYFROWANIA, ZMIENNA k KOLEJNE POLA LANCUCHA W
                                        //KTORYM BEDZIE ZAPISANY ZASZYFROWANY TEKST, ZMIENNA j KOLEJNE ELEMENTY
                                        //KLUCZA
{
    if((tekst[i]<65)||(tekst[i]>90 && tekst[i]<97)||(tekst[i]>122)) // WARUNEK W KTRYM: JESLI ZNAK TEKSTU NIE JEST ANI
    {                                                               // MALA ANI DUZA LITER� W ZASZYFROWANYM TEKSCIE
       szyfr[k]=tekst[i];                                           // POZOSTANIE BEZ ZMIAN
       k++;
       j--;
    };
    if(tekst[i]>=65 && tekst[i]<=90)// WARUNEK WYLAPUJACY DUZE LITERY TEKSTU DO ZASZYFROWANIA
    {
        if(klucz[j]>=65 && klucz[j]<=90)
        {
        wk=klucz[j]-65;
        kt=tekst[i]-65;
        szyfr[k]=tab[wk][kt];
        k++;
        };
         if(klucz[j]>=97 && klucz[j]<=122)
        {
        wk=klucz[j]-97;
        kt=tekst[i]-65;
        szyfr[k]=tab[wk][kt];
        k++;
        };
         if(j==strlen(klucz))
        {
        j=-1;
        i=i-1;
        }
    }
    if(tekst[i]>=97 && tekst[i]<=122)// WARUNEK WYLAPUJACY MALE LITERY TEKSTU DO ZASZYFROWANIA
    {
        if(klucz[j]>=97 && klucz[j]<=122)
        {
        wk=klucz[j]-97;
        kt=tekst[i]-97;
        szyfr[k]=tab[wk][kt];
        k++;
        };
        if(klucz[j]>=65 && klucz[j]<=90)
        {
        wk=klucz[j]-65;
        kt=tekst[i]-97;
        szyfr[k]=tab[wk][kt];
        k++;
        };
         if(j==strlen(klucz))
        {
        j=-1;
        i=i-1;
        }
    }

}
fpszyfr=fopen("szyfr.txt","w");//OTWARCIE PLIKU SZYFR W TRYBIE ZAPISU
if(fpszyfr==NULL)fprintf(stderr,"Blad otwarcia pliku\n");//SPRAWDZENIE POPRAWNOSCI OTWARCIA
else for(i=0;i<strlen(szyfr);i++)//WYPISYWANIE ZASZYFROWANEGO TEKSTU DO PLIKU SZYFR
fprintf(fpszyfr,"%c",szyfr[i]);
fclose(fpszyfr);//ZAMKNIECIE PLIKU SZYFR
}
void odwroceniehasla(void)//FUNKCJA "ODWRACAJACA" HASLO
{
char tabd[]="ABCDEFGHIJKLMNOPQRSTUVWXYZ";//TABLICE POTRZEBNE DO ODWRACANIA HASLA
char tabm[]="abcdefghijklmnopqrstuvwxyz";
char doodw[S];//TABLICA W KTOREJ ZAPISYWANE JEST HASLO DO ODWROCENIA

fpklucz=fopen("klucz.txt","r");//OTWARCIE PLIKU KLUCZ KTORY ZAWIERA HASLO DO ODWROCENIA
if(fpklucz==NULL)fprintf(stderr,"Blad otwarcia pliku\n");//SPRAWDZENIE POPRAWNOSCI OTWARCIA
else fgets(doodw,sizeof(klucz),fpklucz);//ZAPISANIE HASLA Z PLIKU KLUCZ DO TABLICY doodw
fclose(fpklucz);//ZAMKNIECIE PLIKU
int iod;
for(j=0;j<strlen(doodw);j++)
{
    for(i=0;i<strlen(tabm);i++)//PETLA POROWNUJACA ZNAKI Z HASLA Z ZNAKAMI Z TABELI
    {
    if(doodw[j]>=97 && doodw[j]<=122)
        {
        if(tabm[i]==doodw[j])break;
        }
    if(doodw[j]>=65 && doodw[j]<=90)
        {
        if(tabd[i]==doodw[j])break;
        }
    if((doodw[j]<65)||(doodw[j]>90 && doodw[j]<97)||(doodw[j]>122))
        {
        klucz[j]=doodw[j];
        j++;
        }
    }
    iod=(26-i)%26;//WZOR NA PODSTAWIE KTOREGO MA MIEJSCE ODWROCENIE HASLA
    klucz[j]=tabm[iod];
}
}
void deszyfrowanie(void)//FUNKCJA DESZYFRUJACA
{
    odwroceniehasla();//WYWOLANIE FUNKCJI "ODWRACAJACEJ" HASLO
    szyfrowanie();//WYWOLANIE FUNKCJI SZYFRUJACEJ
}

int main()
{
char znak;
printf("Podaj co chcesz zrobic:\n");//WYSWITLENIE OPCJI PROGRAMU
printf("1 - Zaszyfrowac\n");
printf("2 - Odszyfrowac\n");
scanf("%c",&znak);

fpklucz=fopen("klucz.txt","r");                             //OTWARCIE PLIKU ZAWIERAJACEGO
if(fpklucz==NULL)fprintf(stderr,"Blad otwarcia pliku\n");   //KLUCZ POTRZEBNY DO SZYFROWANIA
else fgets(klucz,sizeof(klucz),fpklucz);
fclose(fpklucz);

switch(znak)//WZBOR FUNKCJI PROGRAMU
    {
    case '1': szyfrowanie();break;
    case '2': deszyfrowanie();break;
    }
printf("Zaszyfrowany/Odszyfrowany Tekst:\n%s",szyfr);//WYSWIETLENIE WYNIKU PROGRAMU
    return 0;
}
